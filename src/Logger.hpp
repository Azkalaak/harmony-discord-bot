#pragma once

#include <format>
#include <unistd.h>
#include <mutex>
#include <array>
#include <string_view>
#include <iostream>

using namespace std::literals;

namespace BotHarmo {
   
   enum LogLevel {
      CRITICAL = 0,
      ERROR = 1,
      WARNING = 2,
      INFO = 3,
      DEBUG = 4
   };

   class Color {
   private:
      bool _isTTY;
   public:
      Color(int fd) { _isTTY = isatty(fd); }

      std::string_view red()     { if (_isTTY) return "\033[31m"; return ""; }
      std::string_view green()   { if (_isTTY) return "\033[32m"; return ""; }
      std::string_view yellow()  { if (_isTTY) return "\033[33m"; return ""; }
      std::string_view blue()    { if (_isTTY) return "\033[34m"; return ""; }
      std::string_view grey()    { if (_isTTY) return "\033[90m"; return ""; }
      std::string_view reset()   { if (_isTTY) return "\033[0m";  return ""; }
   };

   class Logger {
   private:
      template<LogLevel lvl>
      constexpr std::string_view toString()
      {
         if constexpr (lvl == CRITICAL)
            return "!!!";
         else if constexpr (lvl == ERROR)
            return " ! ";
         else if constexpr (lvl == WARNING)
            return "???";
         else if constexpr (lvl == INFO)
            return " * ";
         else if constexpr (lvl == DEBUG)
            return " . ";
         else
            static_assert("unknown level.");
         return "";
      }

      template<LogLevel lvl>
      std::string_view toColor()
      {
         if constexpr (lvl == CRITICAL)
            return out.red();
         else if constexpr (lvl == ERROR)
            return out.red();
         else if constexpr (lvl == WARNING)
            return out.yellow();
         else if constexpr (lvl == INFO)
            return out.green();
         else if constexpr (lvl == DEBUG)
            return out.grey();
         else
            static_assert("unknown level.");
         return "";
      }

   static inline LogLevel _lvl { INFO };
   Color out { 1 };
   static inline std::mutex _printTex;
   size_t _progressStatus { 0 };
   static constexpr std::array _animation {
      "   "sv,
      ".  "sv,
      ".. "sv,
      "..."sv,
      " .."sv,
      "  ."sv,
   };

   public:
      template<LogLevel lvl>
      void setLevel() {
         _lvl = lvl;
      }

      template<LogLevel lvl, typename ...Args>
      void log(std::format_string<Args...> fmt, Args&&... args) {
         if (lvl > _lvl)
            return;
         auto res = std::format("[{}{}{}] {}\n", toColor<lvl>(), toString<lvl>(), out.reset(), std::format(fmt, std::forward<Args>(args)...));
         {
            std::lock_guard<std::mutex> _(_printTex);
            std::cout << res;
         }
      }

      // progress bar
      template<typename ...Args>
      void progress(std::format_string<Args...> fmt, Args&&... args) {
         if (_lvl != INFO)
            return; // outside of info, progress bar would get obliterated.
         auto res = std::format("{}[{}] {}", '\r', _animation[_progressStatus], std::format(fmt, std::forward<Args>(args)...));
         {
            std::lock_guard<std::mutex> _(_printTex);
            std::cout << res << std::flush;
         }
         ++_progressStatus;
         if (_progressStatus >= _animation.size())
            _progressStatus = 0;
      }
   };
}
