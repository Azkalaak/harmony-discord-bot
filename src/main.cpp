#include "Logger.hpp"
#include <dpp/appcommand.h>
#include <dpp/dispatcher.h>
#include <dpp/dpp.h>
#include <cstdlib>
#include <dpp/message.h>
#include <dpp/misc-enum.h>
#include <dpp/snowflake.h>
#include <variant>
#include <vector>
#include <unordered_map>

using namespace BotHarmo;

struct Player {
	std::string nom;
  std::string prenom;
  int age {-1};
  std::string image;

  // osef pour l'instant
	std::vector<std::string> inventaire;
	int money {0};
  
};

// in reality, we would need one per server but actually who cares?
std::unordered_map<dpp::snowflake, std::vector<Player>> characters;

void createPersonnage(dpp::cluster &bot, auto &event)
{
  Player p;
  auto prenom = event.get_parameter("prenom");
  auto nom = event.get_parameter("nom");
  auto age = event.get_parameter("age");
  auto image = event.get_parameter("image");
  
  if (std::holds_alternative<std::string>(prenom))
    p.prenom = std::get<std::string>(prenom);
  if (std::holds_alternative<std::string>(nom))
    p.nom = std::get<std::string>(nom);
  if (std::holds_alternative<int64_t>(age))
    p.age = std::get<int64_t>(age);
  if (std::holds_alternative<std::string>(image))
    p.image = std::get<std::string>(image);
  if (p.age != -1 && p.age < 14) {
    event.reply(dpp::message("Pas d'age en dessous de 14 ans.").set_flags(dpp::m_ephemeral));
    bot.log(dpp::ll_warning, "Personnage trop jeune.");
    return;
  }
  auto user = event.command.get_issuing_user();
  if (user.is_bot()) {
    bot.log(dpp::ll_warning, "A bot tried to use ourselved.");
    event.reply();
    return;
  }
  auto userId = user.id;
  if (characters.contains(userId)) {
    for (auto &pv : characters[userId])
      if (pv.prenom == p.prenom) {
        event.reply(dpp::message("Un personnage avec le même nom existe déjà!").set_flags(dpp::m_ephemeral));
        return;
      }
  }
  auto fmt = std::format("Nouveau personnage! Prenom {}, nom: {}, age: {}, img: {}", p.prenom, p.nom, p.age, p.image);
  event.reply(fmt);
  bot.log(dpp::ll_info, fmt);
  characters[userId].emplace_back(p);
}

void listPersonnage(dpp::cluster &, auto &event)
{
  auto user = event.command.get_issuing_user().id;
  std::string charList;
  if (characters.contains(user))
    for (auto &p : characters[user])
      charList += p.prenom + '\n';
  if (charList.empty())
    charList = "Aucun personnage répertorié.";
  else
   charList = "Personnages enregistrés:\n" + charList;
  event.reply(charList);
}

int main() {
	auto tok = std::getenv("BOT_TOKEN");
  Logger logger;

  if (!tok) {
    logger.log<CRITICAL>("Bot token is not set!");
    return 1;
  }

  dpp::cluster bot(std::getenv("BOT_TOKEN"));
  bot.on_log([&](dpp::log_t event)
      {
        if (event.severity == dpp::ll_debug)
          logger.log<DEBUG>("{}", event.message);
        else if (event.severity == dpp::ll_info)
          logger.log<INFO>("{}", event.message);
        else if (event.severity == dpp::ll_warning)
          logger.log<WARNING>("{}", event.message);
        else if (event.severity == dpp::ll_error)
          logger.log<ERROR>("{}", event.message);
        else if (event.severity == dpp::ll_critical)
          logger.log<CRITICAL>("{}", event.message);
      });

	bot.on_slashcommand([&](auto event) {
    auto c = event.command.get_command_name();
		if (c == "ping")
			event.reply("Pong!");
		else if (c == "personnage")
      createPersonnage(bot, event);
    else if (c == "liste")
      listPersonnage(bot, event);
	});

	bot.on_ready([&bot](auto) {
		if (dpp::run_once<struct register_bot_commands>()) {
      dpp::slashcommand ping("ping", "Ping pong", bot.me.id);
      dpp::slashcommand createCharacter("personnage", "créer un nouveau personnage", bot.me.id);
      createCharacter.add_option(dpp::command_option(dpp::co_string, "prenom", "Le prénom du personnage (ex: 'jean' pour Jean Bombeur)", true));
      createCharacter.add_option(dpp::command_option(dpp::co_string, "nom", "Le nom du personnage (ex: 'Bombeur' pour Jean Bombeur)", false));
      createCharacter.add_option(dpp::command_option(dpp::co_integer, "age", "L'age du personnage en années (ex: 18 ans)", false));
      createCharacter.add_option(dpp::command_option(dpp::co_string, "image", "le lien d'une image correspondant au personnage", false));
      dpp::slashcommand listCharacter("liste", "lister mes personnages", bot.me.id);

			bot.global_bulk_command_create({
          ping,
          createCharacter,
          listCharacter
          });
		}
	});

	bot.start(dpp::st_wait);
	return 0;
}
